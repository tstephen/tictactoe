﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Private Members

        /// <summary>
        /// Hold the current result of the cell i nthe active game
        /// </summary>
        private MarkType[] mResults;

        /// <summary>
        /// True if it's Player1's Turn
        /// </summary>
        private bool mPlayer1Turn;

        /// <summary>
        /// True if the game is ended
        /// </summary>
        private bool mGameEnded; 

        #endregion

        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            NewGame();
        }

        #endregion

        /// <summary>
        /// Starts a new game and clears all values back to the start
        /// </summary>
        private void NewGame()
        {
            // Create a new blank array of free cells
            mResults = new MarkType[9];


            // Explicitly set the array to Free
            for (var i = 0; i < mResults.Length; i++)
            {
                mResults[i] = MarkType.Free;
            }

            // Player 1 initial turn
            mPlayer1Turn = true;

            // Iterate every button on the Grid
            Container.Children.Cast<Button>().ToList().ForEach(button =>
            {
                button.Content = string.Empty;
                button.Background = Brushes.White;
                button.Foreground = Brushes.DarkCyan;
            });

            //Make sure the game hasn't finished
            mGameEnded = false;
        }

        /// <summary>
        /// Handles a button click event
        /// </summary>
        /// <param name="sender"> The button that was clicked </param>
        /// <param name="e"> The events of the click </param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (mGameEnded)
            {
                NewGame();
                return;
            }

            var button = (Button)sender;

            var column = Grid.GetColumn(button);
            var row = Grid.GetRow(button);

            var index = column + (3 * row);

            if (mResults[index] != MarkType.Free)
            {
                MessageBox.Show("That cell is already in use!");
                return;
            }

            if (mPlayer1Turn)
            {
                button.Foreground = Brushes.DarkCyan;
                button.Content = "X";
                mResults[index] = MarkType.Cross;
                mPlayer1Turn = false;
            } else
            {
                button.Foreground = Brushes.MediumVioletRed;
                button.Content = "O";
                mResults[index] = MarkType.Nought;
                mPlayer1Turn = true;
            }

            var isEnded = gameEnded();
            if (isEnded != MarkType.Free)
            {
                MessageBox.Show($"Game Ended. Winner Player: {(isEnded == MarkType.Cross ? 'X' : 'O')}. Click on a cell to continue.");
                mGameEnded = true;
                return;
            }

            bool fullGrid = true;
            foreach(var i in mResults)
            {   
                if (i == MarkType.Free)
                {
                    fullGrid = false;
                }
            }

            if (fullGrid)
            {
                MessageBox.Show("Game Ended. Result: Draw. Click any cell to continue.");
                mGameEnded = true;
                return;
            }
        }

        private MarkType gameEnded()
        {
            // Check horizontal
            for (int i = 0; i < 3; i++)
            {
                var index = (3 * i);
                if ((mResults[index] == mResults[index + 1] && mResults[index + 1] == mResults[index + 2]) && mResults[index] != MarkType.Free)
                {
                    return mResults[index];
                }
            }

            // Check Vertical
            for (int i = 0; i < 3; i++)
            {
                if ((mResults[i] == mResults[i + 3] && mResults[i] == mResults[i + 6]) && mResults[i] != MarkType.Free)
                {
                    return mResults[i];
                }
            }
            
            // Check Diagonal
            if ((mResults[0] == mResults[4] && mResults[0] == mResults[8]) && mResults[0] != MarkType.Free)
            {
                return mResults[0];
            }

            if ((mResults[2] == mResults[4] && mResults[2] == mResults[6]) && mResults[2] != MarkType.Free)
            {
                return mResults[2];
            }

            return MarkType.Free;
        }
    }
}
